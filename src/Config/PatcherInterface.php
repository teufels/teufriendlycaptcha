<?php

declare(strict_types=1);

namespace TeuFriendlyCaptcha\Config;

use JsonException;

/**
 * interface for third party decoration purposes.
 */
interface PatcherInterface
{
    /**
     * @throws JsonException
     */
    public function patchValue(array $configPresent, array $data): array;
}
