<?php

declare(strict_types=1);

namespace TeuFriendlyCaptcha\Config;

use Doctrine\DBAL\Connection;
use Exception;
use JsonException;
use TeuFriendlyCaptcha\Exception\MissingActiveCaptchasConfigException;

/**
 * interface for third party decoration purposes.
 */
interface ManagerInterface
{
    public function __construct(Patcher $patcher);

    public static function create(): self;

    /**
     * @throws Exception
     * @throws MissingActiveCaptchasConfigException
     * @throws JsonException
     */
    public function updateConfig(Connection $connection, array $data): void;
}
