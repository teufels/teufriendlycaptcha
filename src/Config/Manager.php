<?php

declare(strict_types=1);

namespace TeuFriendlyCaptcha\Config;

use Doctrine\DBAL\Connection;
use Exception;
use JsonException;
use TeuFriendlyCaptcha\Exception\MissingActiveCaptchasConfigException;

/**
 * we have to manage the config ourselves because setting it using the SystemConfigService wouldn't work.
 */
class Manager implements ManagerInterface
{
    private PatcherInterface $patcher;

    public function __construct(PatcherInterface $patcher)
    {
        $this->patcher = $patcher;
    }

    public static function create(): self
    {
        return new self(new Patcher());
    }

    /**
     * @throws Exception
     * @throws MissingActiveCaptchasConfigException
     * @throws JsonException
     */
    public function updateConfig(Connection $connection, array $data): void
    {
        // fetch existing config to edit it
        $configPresent = $connection->fetchAssociative(
            'select * from `system_config` where `configuration_key` = ?',
            ['core.basicInformation.activeCaptchasV2']
        );

        if (false === $configPresent) {
            // the active captchas config should not be missing at this point
            throw new MissingActiveCaptchasConfigException();
        }

        $value = $this->patcher->patchValue($configPresent, $data);

        // persist patched data
        $connection->update('system_config', [
            /* @infection-ignore-all */
            'configuration_value' => json_encode([
                '_value' => $value,
            ], JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE),
        ], [
            'id' => $configPresent['id'],
        ]);
    }
}
