<?php

declare(strict_types=1);

namespace TeuFriendlyCaptcha\Config;

use JsonException;
use TeuFriendlyCaptcha\Storefront\Framework\Captcha\FriendlyCaptcha;

class Patcher implements PatcherInterface
{
    /**
     * @throws JsonException
     */
    public function patchValue(array $configPresent, array $data): array
    {
        // patch friendly captcha configuration into system config
        /** @infection-ignore-all */
        $configurationValue = json_decode($configPresent['configuration_value'], true, 512, JSON_THROW_ON_ERROR);
        $value = $configurationValue['_value'];

        if ([] === $data) {
            // delete config
            unset($value[FriendlyCaptcha::CAPTCHA_NAME]);
        } elseif (!\array_key_exists(FriendlyCaptcha::CAPTCHA_NAME, $value)) {
            // create config
            $value[FriendlyCaptcha::CAPTCHA_NAME] = $data;
        } else {
            // merge config
            $value[FriendlyCaptcha::CAPTCHA_NAME] = $this->merge($value[FriendlyCaptcha::CAPTCHA_NAME], $data);
        }

        return $value;
    }

    private function merge(array $value, array $data): array
    {
        foreach ($data as $key => $item) {
            if (\is_array($item) && \array_key_exists($key, $value)) {
                $value[$key] = $this->merge($value[$key], $item);

                continue;
            }

            $value[$key] = $item;
        }

        return $value;
    }
}
