<?php declare(strict_types=1);

namespace TeuFriendlyCaptcha\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;
use TeuFriendlyCaptcha\Config\Manager;
use TeuFriendlyCaptcha\Exception\MissingActiveCaptchasConfigException;

class Migration1690902755FriendlyCaptchaVersionUpdate extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1690902755;
    }

    /**
     * @throws MissingActiveCaptchasConfigException
     * @throws \JsonException
     */
    public function update(Connection $connection): void
    {
        Manager::create()->updateConfig($connection, [
            'version' => '0.9.13',
        ]);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
