<?php declare(strict_types=1);

namespace TeuFriendlyCaptcha\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;
use TeuFriendlyCaptcha\Config\Manager;
use TeuFriendlyCaptcha\Storefront\Framework\Captcha\FriendlyCaptcha;

class Migration1664446648 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1_664_446_648;
    }

    public function update(Connection $connection): void
    {
        Manager::create()->updateConfig($connection, [
            'name' => FriendlyCaptcha::CAPTCHA_NAME,
            'isActive' => false,
            'config' => [
                'siteKey' => '',
                'secretKey' => '',

                // possible options:
                // - auto
                // - focus
                // - none
                'startMode' => 'focus',

                // possible options:
                // - jsdelivr
                // - unpgk
                // - self-hosted
                'deliveryMethod' => 'jsdelivr',

                // possible options:
                // - europe
                // - global
                'endpointSelection' => 'europe',
                'endpointSelectionFallback' => true,
                'theme' => [
                    // possible options:
                    // - light (default)
                    // - dark (styling by friendly captcha)
                    // - custom (custom css)
                    'name' => 'light',

                    // will contain link to custom css file
                    'customStyleFile' => '',
                ],
                'polyfill' => false,
            ],
            // this will be used in templates for cdn delivery
            // this field is not changeable in the administration
            'version' => '0.9.4',
        ]);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
