<?php

declare(strict_types=1);

namespace TeuFriendlyCaptcha\Exception;

class MissingActiveCaptchasConfigException extends \Exception
{
}
