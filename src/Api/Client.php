<?php

declare(strict_types=1);

namespace TeuFriendlyCaptcha\Api;

use GuzzleHttp\ClientInterface as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\ResponseInterface;

class Client implements ClientInterface
{
    public const FRIENDLY_CAPTCHA_VERIFY_ENDPOINT = 'https://api.friendlycaptcha.com/api/v1/siteverify';
    private HttpClient $client;

    public function __construct(HttpClient $client)
    {
        $this->client = $client;
    }

    /**
     * @throws \JsonException
     */
    public function isValidSolution(string $solution, string $secretKey, string $siteKey): bool
    {
        try {
            $response = $this->getValidationResponse($solution, $secretKey, $siteKey);

            return $response['success'];
        } catch (ClientExceptionInterface $exception) {
            // reference: https://docs.friendlycaptcha.com/#/installation?id=verification-best-practices
            return true;
        }
    }

    /**
     * @throws GuzzleException
     * @throws \JsonException
     */
    public function getValidationResponse(string $solution, string $secretKey, string $siteKey): array
    {
        /** @see https://docs.friendlycaptcha.com/#/installation */
        $response = $this->client->request('POST', self::FRIENDLY_CAPTCHA_VERIFY_ENDPOINT, [
            'form_params' => [
                'solution' => $solution,
                'secret' => $secretKey,
                'sitekey' => $siteKey,
            ],
        ]);

        return $this->decodeResponseBody($response);
    }

    /**
     * @throws \JsonException
     */
    public function decodeResponseBody(ResponseInterface $response): array
    {
        $responseRaw = $response->getBody()->getContents();

        /* @infection-ignore-all */
        return json_decode($responseRaw, true, 512, JSON_THROW_ON_ERROR);
    }
}
