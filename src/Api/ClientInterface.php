<?php

declare(strict_types=1);

namespace TeuFriendlyCaptcha\Api;

use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

/**
 * interface for third party decoration purposes.
 */
interface ClientInterface
{
    /**
     * @throws \JsonException
     */
    public function isValidSolution(string $solution, string $secretKey, string $siteKey): bool;

    /**
     * @throws GuzzleException
     * @throws \JsonException
     */
    public function getValidationResponse(string $solution, string $secretKey, string $siteKey): array;

    /**
     * @throws \JsonException
     */
    public function decodeResponseBody(ResponseInterface $response): array;
}
