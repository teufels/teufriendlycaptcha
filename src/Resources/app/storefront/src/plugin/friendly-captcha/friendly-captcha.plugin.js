import Plugin from 'src/plugin-system/plugin.class';
import {WidgetInstance as DefaultWidget} from '@friendly-challenge';
// @see https://docs.friendlycaptcha.com//browser_support?id=compatability-mode-for-the-library
import {WidgetInstance as PolyfillWidget} from '@friendly-challenge/compat';
import DomAccess from 'src/helper/dom-access.helper';

export default class FriendlyCaptchaPlugin extends Plugin {
  static options = {
    startMode: null,
    language: null,
    endpointSelection: null,
    endpointSelectionFallback: null,
    deliveryMethod: null,
    polyfill: null,
  };

  init() {
      const wrapper = document.getElementById('teuFriendlyCaptchaWrapper');
      const form = wrapper.closest('form')
      const btn = DomAccess.querySelector(form, 'button[type="submit"]', false);
    const widgetClass = this.findWidgetClass();
    const widget = new widgetClass(this.el, {
      startMode: this.options.startMode,
      language: this.findLanguageCode(),
      puzzleEndpoint: this.findPuzzleEndpoint(),
      doneCallback: doneCallback,
    });
      function doneCallback(solution) {
          btn.disabled = false;
      }

      btn.disabled = true;
    widget.init();
  }

  /**
   * @returns {WidgetInstance}
   */
  findWidgetClass() {
    // polyfills are included elsewhere
    // the widget class is the same for all cdn inclusions
    if ('self-hosted' !== this.options.deliveryMethod) {
      return DefaultWidget;
    }

    if (this.options.polyfill) {
      return PolyfillWidget;
    }

    return DefaultWidget;
  }

  /**
   * @returns {string}
   */
  findLanguageCode() {
    const supported = [
      'en', 'fr', 'de', 'it', 'nl', 'pt', 'es', 'ca', 'da',
      'ja', 'el', 'uk', 'bg', 'cs', 'sk', 'no', 'fi', 'lt',
      'pl', 'et', 'hr', 'sr', 'sl', 'hu', 'ro', 'nb', 'zh',
    ];

    if (supported.includes(this.options.language)) {
      return this.options.language;
    }

    // friendly captcha will use 'en' as default value,
    // if the language is not supported.
    // by passing 'en' manually, we prevent an error from being thrown
    return 'en';
  }

  /**
   * @returns {string}
   */
  findPuzzleEndpoint() {
    const global = 'https://api.friendlycaptcha.com/api/v1/puzzle';

    // in case anything breaks, the global endpoint will still be used.
    if ('europe' !== this.options.endpointSelection) return global;

    const europe = 'https://eu-api.friendlycaptcha.eu/api/v1/puzzle';

    // @see https://docs.friendlycaptcha.com//eu_endpoint?id=fallback-to-global-service
    if (this.options.endpointSelectionFallback) return `${europe},${global}`;

    return europe;
  }
}
