import FriendlyCaptchaPlugin from './plugin/friendly-captcha/friendly-captcha.plugin';

const PluginManager = window.PluginManager;

PluginManager.register(
    'FriendlyCaptchaPlugin',
    FriendlyCaptchaPlugin,
    '[data-friendly-captcha-plugin]',
);

if (module.hot) {
  module.hot.accept();
}
