const {join, resolve} = require('path');

module.exports = () => {
  return {
    resolve: {
      alias: {
        '@friendly-challenge': resolve(
            join(
                __dirname,
                '..',
                'node_modules',
                'friendly-challenge',
            ),
        ),
      },
    },
  };
};
