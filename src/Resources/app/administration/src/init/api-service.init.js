import FriendlyCaptchaApiCredentialsService from '../core/service/api/friendly-captcha-api-credentials.service';

const {Application} = Shopware;

const initContainer = Application.getContainer('init');

// register service to di container
Application.addServiceProvider(
    'friendlyCaptchaApiCredentialsService',
    (container) => new FriendlyCaptchaApiCredentialsService(
        initContainer.httpClient,
        container.loginService,
    ),
);
