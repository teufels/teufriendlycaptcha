const ApiService = Shopware.Classes.ApiService;

/**
 * @see https://github.com/shopwareLabs/SwagPayPal/blob/f3d830e24a4e4f068662941fe9931f8c28b4067b/
 */
export default class FriendlyCaptchaApiCredentialsService extends ApiService {
  constructor(httpClient, loginService, apiEndpoint = 'friendly-captcha') {
    super(httpClient, loginService, apiEndpoint);
  }

  /**
   * @param {string} siteKey
   * @param {string} secretKey
   * @returns promise
   */
  validateApiCredentials(siteKey, secretKey) {
    const headers = this.getBasicHeaders();

    return this.httpClient.get(
        `_action/${this.getApiBasePath()}/validate-api-credentials`,
        {
          params: {siteKey, secretKey},
          headers: headers,
        },
    ).then((response) => {
      return ApiService.handleResponse(response);
    });
  }
}
