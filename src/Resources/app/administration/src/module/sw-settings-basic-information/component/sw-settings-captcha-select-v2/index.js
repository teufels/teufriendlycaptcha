import template from './sw-settings-captcha-select-v2.html.twig';
import deDE from './../../snippet/de-DE';
import enGB from './../../snippet/en-GB';

Shopware.Component.override('sw-settings-captcha-select-v2', {
  template,

  inject: ['friendlyCaptchaApiCredentialsService'],

  mixins: [
    'notification',
  ],

  snippets: {
    'de-DE': deDE,
    'en-GB': enGB,
  },

  data() {
    return {
      credentialsValid: false,
      isValidatingCredentials: false,
    };
  },

  computed: {
    themeOptions() {
      return [
        {
          label: this.$tc(
              'sw-settings-basic-information.captcha.label' +
              '.friendlyCaptchaThemeLight',
          ),
          value: 'light',
        },
        {
          label: this.$tc(
              'sw-settings-basic-information.captcha.label' +
              '.friendlyCaptchaThemeDark',
          ),
          value: 'dark',
        },
        {
          label: this.$tc(
              'sw-settings-basic-information.captcha.label' +
              '.friendlyCaptchaThemeCustom',
          ),
          value: 'custom',
        },
      ];
    },

    startModeOptions() {
      return [
        {
          label: this.$tc(
              'sw-settings-basic-information.captcha.label' +
              '.friendlyCaptchaStartModeAutomatic',
          ),
          value: 'auto',
        },
        {
          label: this.$tc(
              'sw-settings-basic-information.captcha.label' +
              '.friendlyCaptchaStartModeFocus',
          ),
          value: 'focus',
        },
        {
          label: this.$tc(
              'sw-settings-basic-information.captcha.label' +
              '.friendlyCaptchaStartModeManual',
          ),
          value: 'none',
        },
      ];
    },

    deliveryMethodOptions() {
      return [
        {
          label: this.$tc(
              'sw-settings-basic-information.captcha.label' +
              '.friendlyCaptchaDeliveryMethodJsDelivr',
          ),
          value: 'jsdelivr',
        },
        {
          label: this.$tc(
              'sw-settings-basic-information.captcha.label' +
              '.friendlyCaptchaDeliveryMethodUNPKG',
          ),
          value: 'unpkg',
        },
        {
          label: this.$tc(
              'sw-settings-basic-information.captcha.label' +
              '.friendlyCaptchaDeliveryMethodSelfHosted',
          ),
          value: 'self-hosted',
        },
      ];
    },

    endpointSelectionOptions() {
      return [
        {
          label: this.$tc(
              'sw-settings-basic-information.captcha.label' +
              '.friendlyCaptchaEndpointSelectionEurope',
          ),
          value: 'europe',
        },
        {
          label: this.$tc(
              'sw-settings-basic-information.captcha.label' +
              '.friendlyCaptchaEndpointSelectionGlobal',
          ),
          value: 'global',
        },
      ];
    },
  },

  methods: {
    potentialCaptchaConflict() {
      // find out if friendly captcha is active together with any other option
      for (const [key, value] of Object.entries(this.currentValue)) {
        if ('friendlyCaptcha' !== key && value.isActive) {
          return this.currentValue.friendlyCaptcha.isActive;
        }
      }

      return false;
    },

    captchaSettingsAreGdprCompliant() {
      // google's recaptcha is not gdpr compliant as of 04/03/22
      return !this.currentValue.googleReCaptchaV3.isActive &&
          !this.currentValue.googleReCaptchaV2.isActive;
    },

    validateApiCredentials() {
      // start button loading animation
      this.isValidatingCredentials = true;

      // @see https://github.com/shopwareLabs/SwagPayPal/blob/f3d830e24a4e4f068662941fe9931f8c28b4067b/
      this.friendlyCaptchaApiCredentialsService.validateApiCredentials(
          this.currentValue.friendlyCaptcha.config.siteKey,
          this.currentValue.friendlyCaptcha.config.secretKey,
      ).then(this.onValidationResponse).catch(this.onValidationError);
    },

    onValidationResponse(response) {
      this.credentialsValid = response.credentialsValid;

      if (true === this.credentialsValid) {
        // credentials are valid
        this.createNotificationSuccess({
          message: this.$tc(
              'sw-settings-basic-information.captcha.notification.' +
              'validCredentials',
          ),
        });
      } else {
        // credentials are invalid
        this.createNotificationError({
          message: this.$tc(
              'sw-settings-basic-information.captcha.notification.' +
              'invalidCredentials',
          ),
        });
      }

      // stop button loading animation
      this.isValidatingCredentials = false;
    },

    onValidationError(errorResponse) {
      // credential validation failed
      this.createNotificationError({
        message: this.$tc(
            'sw-settings-basic-information.captcha.notification.' +
            'errorInCredentialsValidation',
        ),
      });

      this.credentialsValid = errorResponse.credentialsValid;

      // stop button loading animation
      this.isValidatingCredentials = false;
    },

    validKeyLengths() {
      // default friendly captcha key lengths
      return 16 === this.currentValue.friendlyCaptcha.config.siteKey.length &&
          58 === this.currentValue.friendlyCaptcha.config.secretKey.length;
    },
  },
});
