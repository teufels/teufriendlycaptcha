<?php

declare(strict_types=1);

namespace TeuFriendlyCaptcha;

use Doctrine\DBAL\Connection;
use TeuFriendlyCaptcha\Config\Manager;
use TeuFriendlyCaptcha\Config\ManagerInterface;
use TeuFriendlyCaptcha\Exception\MissingActiveCaptchasConfigException;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\DeactivateContext;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;

class TeuFriendlyCaptcha extends Plugin
{
    final public const CONFIG_KEY = 'core.basicInformation.activeCaptchasV2.friendlyCaptcha';

    /**
     * @throws MissingActiveCaptchasConfigException
     * @throws \JsonException
     */
    public function deactivate(DeactivateContext $deactivateContext): void
    {
        // disable friendly captcha on deactivation because shopware would
        // try to load the captcha template and break otherwise
        $this->getManager()->updateConfig($this->getConnection(), [
            'isActive' => false,
        ]);
    }

    protected function getConnection(): Connection
    {
        $connection = $this->container->get(Connection::class);

        if (!$connection instanceof Connection) {
            throw new \RuntimeException();
        }

        return $connection;
    }

    protected function getManager(): ManagerInterface
    {
        $manager = $this->container->get(ManagerInterface::class);

        if (!$manager instanceof ManagerInterface) {
            throw new \RuntimeException();
        }

        return $manager;
    }

    /**
     * @throws MissingActiveCaptchasConfigException
     * @throws \JsonException
     */
    public function uninstall(UninstallContext $uninstallContext): void
    {
        // since the template loading issue has been fixed in $this->deactivate(),
        // we must only check for data removal here
        if ($uninstallContext->keepUserData()) {
            return;
        }

        // we want to cleanup our data after the plugin was uninstalled
        // this is important to make sure secret keys & other data won't remain in the database
        Manager::create()->updateConfig($this->getConnection(), []);
    }
}
