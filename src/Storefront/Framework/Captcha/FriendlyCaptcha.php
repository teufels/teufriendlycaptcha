<?php

declare(strict_types=1);

namespace TeuFriendlyCaptcha\Storefront\Framework\Captcha;

use TeuFriendlyCaptcha\Api\Client;
use TeuFriendlyCaptcha\Api\ClientInterface;
use Shopware\Storefront\Framework\Captcha\AbstractCaptcha;
use Shopware\Storefront\Framework\Captcha\CaptchaRouteListener;
use Shopware\Storefront\Framework\Captcha\GoogleReCaptchaV3;
use Symfony\Component\HttpFoundation\Request;

/**
 * @see GoogleReCaptchaV3 for reference
 */
class FriendlyCaptcha extends AbstractCaptcha
{
    public const CAPTCHA_NAME = 'friendlyCaptcha';
    public const CAPTCHA_REQUEST_PARAMETER = 'frc-captcha-solution';
    private const CONFIG = 'config';

    private ClientInterface $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function supports(Request $request, array $captchaConfig): bool
    {
        $parent = parent::supports($request, $captchaConfig);

        // request shall only be supported if our config is valid
        $validConfig = $this->isValidConfig($captchaConfig);

        // no check for captcha parameter on purpose because it could be removed by the client

        return $parent && $validConfig;
    }

    private function isValidConfig(array $captchaConfig): bool
    {
        // i don't know why we have to do these checks to this extend.
        // shopware did it in their implementation so that's why im doing it too.
        if (null === $secretKey = $captchaConfig[self::CONFIG]['secretKey']) {
            return false;
        }

        if ('' === $secretKey) {
            return false;
        }

        if (!\is_string($secretKey)) {
            return false;
        }

        if (null === $siteKey = $captchaConfig[self::CONFIG]['siteKey']) {
            return false;
        }

        return '' !== $siteKey && \is_string($siteKey);
    }

    /**
     * @throws \JsonException
     */
    public function isValid(Request $request, array $captchaConfig): bool
    {
        /** @var string|null $solution */
        $solution = $request->request->get(self::CAPTCHA_REQUEST_PARAMETER);

        if (null === $solution) {
            return false;
        }

        // $this->support() gets called first. no need to validate anything here
        /* @see CaptchaRouteListener::validateCaptcha() for reference */

        return $this->client->isValidSolution(
            $solution,
            $captchaConfig[self::CONFIG]['secretKey'],
            $captchaConfig[self::CONFIG]['siteKey']
        );
    }

    public function getName(): string
    {
        return self::CAPTCHA_NAME;
    }
}
