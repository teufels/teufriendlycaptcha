<?php

declare(strict_types=1);

namespace TeuFriendlyCaptcha\Controller;

use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use TeuFriendlyCaptcha\Api\ClientInterface;
use Shopware\Core\Framework\Routing\Annotation\Acl;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Routing\Exception\InvalidRequestParameterException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['storefront']])]
class ApiCredentialsController extends AbstractController
{
    /**
     * @internal
     */
    public const ERRORS = 'errors';

    private ClientInterface $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    #[Route(path: '/api/_action/friendly-captcha/validate-api-credentials', name: 'api.action.friendly_captcha.validate.api.credentials', defaults: ['_routeScope' => ['api'], '_acl' => ['sales_channel.editor']])]
    public function validateApiCredentials(Request $request): JsonResponse
    {
        $secretKey = $request->query->get('secretKey');

        if (!\is_string($secretKey)) {
            throw new InvalidRequestParameterException('secretKey');
        }

        $siteKey = $request->query->get('siteKey');

        if (!\is_string($siteKey)) {
            throw new InvalidRequestParameterException('siteKey');
        }

        $credentialsValid = $this->isValidCredentials($secretKey, $siteKey);

        return new JsonResponse([
            'credentialsValid' => $credentialsValid,
        ]);
    }

    protected function isValidCredentials(string $secretKey, string $siteKey): bool
    {
        try {
            // we're working with an incorrect solution on purpose because
            // we only care about credential validity at this point
            $response = $this->client->getValidationResponse('foo', $secretKey, $siteKey);
        } catch (BadResponseException $exception) {
            $response = $this->client->decodeResponseBody($exception->getResponse());
        } catch (GuzzleException $exception) {
            // these exceptions do not have the $exception->getResponse() method.
            // we force false here
            return false;
        }

        if (!\array_key_exists(0, $response[self::ERRORS])) {
            return false;
        }

        // friendly captcha error response: <field-name>_invalid
        return !\in_array($response[self::ERRORS][0], ['secret_invalid', 'sitekey_invalid'], true);
    }
}
