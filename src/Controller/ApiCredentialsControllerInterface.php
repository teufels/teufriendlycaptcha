<?php

declare(strict_types=1);

namespace TeuFriendlyCaptcha\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * interface for third party decoration purposes.
 */
interface ApiCredentialsControllerInterface
{
    /**
     * @throws \JsonException
     */
    public function validateApiCredentials(Request $request): JsonResponse;
}
